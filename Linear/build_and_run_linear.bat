@ECHO off

ECHO.
WHERE clang.exe
IF %ERRORLEVEL% EQU 0 (
  ECHO clang found!
  ECHO.
) ELSE (
  ECHO no clang! setting env.
  ECHO.
  call vcvarsall.bat x64
)

clang -g -v -Wall -Wextra LinearAllocatorTest.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -DNO_BASE_INCLUDE -fdiagnostics-absolute-paths -Wno-return-stack-address -o LinearAllocatorTestDebug.exe

clang -O3  LinearAllocatorTest.c "-LUser32.lib" "-Lkernel32.lib" "-LGdi32.lib" "-Lshell32.lib" "-Lvcruntime.lib" "-Lmsvcrt.lib" "-Llibcmt.lib" -DNO_BASE_INCLUDE -fdiagnostics-absolute-paths -Wno-return-stack-address -o LinearAllocatorTestRelease.exe


ECHO.


IF EXIST LinearAllocatorTestRelease.exe (
  CALL LinearAllocatorTestRelease.exe
) ELSE (
  ECHO No executable found.
)


GOTO :EOF