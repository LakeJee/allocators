/* 
* Understanding my choice for HeapAlloc here and not something like VirtualAlloc:
* ...
* https://learn.microsoft.com/en-us/windows/win32/memory/comparing-memory-allocation-methods
* The above link explains various allocation methods using the win32 API.
* 
* So for this allocator we just ask the OS to reserve a contiguous block of memory.
* 
* We don't want to have to worry about page granularity or anything like that.
*
* We won't be calling HeapFree or any shit like that becuase WE are handling allocating 
* and deallocating from our own memory block (*base).
* 
* When we are done with the allocator, we will call HeapFree where we pass our Allocator
* pointer to the HeapFree function.
*
* We use HeapCreate(...) and store the returned HANDLE value with each alloctor so each
* allocator can have it's own private heap space.
* (See: https://learn.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapcreate)
* 
*/
function Allocator *
  allocator_init_default (void)
{
  U64 size = Gigabytes (2);
  Allocator *a = 0;
  HANDLE win32HeapHandle = HeapCreate (0, sizeof(Allocator), 0);
  if (!win32HeapHandle) 
  {
    printf("Failed to create private heap HANDLE for allocator use.\n");
    printf("Defaulting to GetProcessHeap() for allocator usage.\n");
    a = (Allocator *) HeapAlloc (GetProcessHeap(), HEAP_ZERO_MEMORY, size);
  } else
  {
    a = (Allocator *) HeapAlloc (win32HeapHandle, HEAP_ZERO_MEMORY, size);
  }
  
  if (!a)
  {
    printf ("Failed to reserve memory block for size : %" PRIu64 "\n", size);
    return a;
  } else
  {
    a->base = (void *) (a + 1);
    a->handle = win32HeapHandle;
    a->next = (U64) a->base;
    a->end = (U64) a->base + size;
    a->used = 0;
    a->totalSize = size;
    a->allocatorFunctions = NULL;
    return (a);
  }
  
}


function Allocator *
  allocator_init (U64 size)
{
  assert (POWER_OF_TWO (size));
  Allocator *a = 0;
  HANDLE win32HeapHandle = HeapCreate (0, sizeof(Allocator), 0);
  if (!win32HeapHandle) 
  {
    printf("Failed to create private heap HANDLE for allocator use.\n");
    printf("Defaulting to GetProcessHeap() for allocator usage.\n");
    a = (Allocator *) HeapAlloc (GetProcessHeap(), HEAP_ZERO_MEMORY, size);
  } else 
  {
    a = (Allocator *) HeapAlloc (win32HeapHandle, HEAP_ZERO_MEMORY, size);
  }
  
  if (!a)
  {
    printf ("Failed to reserve memory block for size : %" PRIu64 "\n", size);
    return a;
  } else
  {
    a->base = (void *) (a + 1);
    a->handle = win32HeapHandle;
    a->next = (U64) a->base;
    a->end = (U64) a->base + size;
    a->used = 0;
    a->totalSize = size;
    a->allocatorFunctions = NULL;
    return (a);
  }
}


/*
* DRAGON HERE:
* Whatever you allocate here will live forever on the arena until it is rolled back
* in the order it was allocated in OR until the arena has been freed.
*
 * ... 
* You have been warned.
*/
function void *
  allocator_alloc_aligned (Allocator * a, U64 size)
{
  U64 *alignedPtr = (U64 *) ALIGN (a->next);
  
  U64 newNext = (U64) alignedPtr + size;
  
  if ((newNext <= a->end) && !(a->used >= a->totalSize))
  {
    // We have enough space to allocate and we return a pointer to the beginning of that buffer
    void *ptr = (void *) alignedPtr;
    a->next = newNext;
    a->used += size;
    return (ptr);
  } else
  {
    printf ("This arena is out of memory. Download more RAM.");
    return (NULL);
  }
}


/*
* DRAGON HERE: 
* We can only rollback in the order of the allocations we have already made!
*
* In theory we can update this and pass in a ***ptr that stores a reference to the pointers
* you have allocated using the arena and then invalidate them by setting them to NULL.
*
* The above might be overkill BUT could be worth as long as we can keep it from getting out of hand.
* 
* ...
* You have been warned.
*/
function void
  allocator_rollback (Allocator * a, U64 size)
{
  // From rjf: the 4 lines of code below will allow us to determine how far to roll back to
  U64 minPos = sizeof (a) + 1;
  U64 sizeToClear = Min (size, a->next);
  U64 newNext = a->next - sizeToClear;
  newNext = Max (newNext, minPos);
  
  if (!(newNext >= (U64) a->base && newNext <= (U64) a->next))
  {
    printf ("Failed to rollback offset to previous position. Aborting.\n");
    printf ("Update this function or rework your application. Good luck.\n");
    abort ();
  } else
  {
    a->next = newNext;
    a->used -= size;
    MemorySet ((U64 *) a->next, 0, size);
  }
}


/*
* https://learn.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapfree
*
* HeapFree will return non-zero if it is successful.
* 
*/
function B8
  allocator_free (Allocator * a)
{
  if (a->handle)
  {
    return (HeapFree (a->handle, 0, a) != 0);
  } else
  {
    return (HeapFree (GetProcessHeap(), 0, a)) != 0;
  }
}


/*
* Utilities
*
*/
function TempAllocator
  allocator_temp_create (Allocator * a)
{
  TempAllocator t = { 0 };
  t.a = a;
  t.used = a->used;
  return (t);
}

function void *
  allocator_temp_allocate (TempAllocator * t, U64 size)
{
  t->used += size;
  return (allocator_alloc_aligned (t->a, size));
}

function void
  allocator_temp_release (TempAllocator t)
{
  allocator_rollback (t.a, t.used);
}

function U64
  allocator_get_remaining (Allocator * a)
{
  return (a->totalSize - a->used);
}

function void 
  assign_function_table (Allocator *a, AllocatorFunctionTable *t)
{
  a->allocatorFunctions = t;
}

function void
  construct_function_table(AllocatorFunctionTable *t, 
                           AllocatorInitDefualtFunction *initDefault,
                           AllocatorInitFunction *init,
                           AllocatorFreeFunction *free,
                           AllocateAlignedFunction *allocate,
                           AllocateRollbackFunction *rollBack,
                           AllocatorCreateTempFunction *createTemp,
                           AllocatorTempAllocateFunction *tempAllocate,
                           AllocatorTempRelaseFunction *tempRelease,
                           AllocatorGetRemainingFunction *getRemaining)
{
  t->initDefault = initDefault;
  t->init = init;
  t->free = free;
  t->allocate = allocate;
  t->rollBack = rollBack;
  t->createTemp = createTemp;
  t->tempAllocate = tempAllocate; 
  t->tempRelease = tempRelease;
  t->getRemaining = getRemaining;
}