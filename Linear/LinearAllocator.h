/* date = April 24th 2023 2:10 pm */

#ifndef _LINEAR_ALLOCATOR_H
#define _LINEAR_ALLOCATOR_H

/* 
* A simple memory arena designed for Win32 applications.
*
* It will be a linear allocator, that can allocate and deallocate in order 
* of the allocations that have already been made.
* 
* Allocator (Change later?)
* void    *base: Pointer to buffer where we make allocations.
* U64      next: Number that represents the next valid *location* in the arena
*                where an allocation can be made.
* U64       end: Number that represents the last valid *lolcation* in the arena
*                where an allocation can be made.
* U64      used: Number that represents *total memory* used by the arena.
* U64 totalSize: Number that represents *total size* of the arena.
* 
*/

/*
* Dependencies
*
* If you don't have a big ass base_include file then you will need to 
* define NO_BASE_INLCUDE either here or in your compiler opts.
*/

#if NO_BASE_INCLUDE 
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdint.h>
#include <assert.h>
#include <inttypes.h>
#endif 

#define function   static
#define MemoryCopy memcpy
#define MemorySet  memset

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define ALIGN(size) (((size) + (DEFAULT_ALIGNMENT - 1)) & ~(DEFAULT_ALIGNMENT - 1))
#define POWER_OF_TWO(x)  (((x) & ((x) - 1)) == 0)

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)

#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))


typedef int8_t   S8;
typedef S8       B8;

typedef struct AllocatorFunctionTable AllocatorFunctionTable;

typedef struct Allocator Allocator;
struct Allocator
{
  void *base;
  HANDLE handle;
  U64 next;
  U64 end;
  U64 used;
  U64 totalSize;
  AllocatorFunctionTable *allocatorFunctions;
};

typedef struct TempAllocator TempAllocator;
struct TempAllocator
{
  Allocator *a;
  U64 used;
};


typedef Allocator* (AllocatorInitDefualtFunction) (void);
typedef Allocator* (AllocatorInitFunction) (U64);
typedef B8         (AllocatorFreeFunction) (Allocator*);
typedef void*      (AllocateAlignedFunction) (Allocator*, U64);
typedef void       (AllocateRollbackFunction) (Allocator*, U64);
typedef TempAllocator (AllocatorCreateTempFunction) (Allocator*);
typedef void*         (AllocatorTempAllocateFunction) (TempAllocator*, U64);
typedef void          (AllocatorTempRelaseFunction) (TempAllocator);
typedef U64           (AllocatorGetRemainingFunction) (Allocator*);

struct AllocatorFunctionTable
{
  AllocatorInitDefualtFunction *initDefault;
  AllocatorInitFunction *init;
  AllocatorFreeFunction *free;
  AllocateAlignedFunction *allocate;
  AllocateRollbackFunction *rollBack;
  AllocatorCreateTempFunction *createTemp;
  AllocatorTempAllocateFunction *tempAllocate;
  AllocatorTempRelaseFunction *tempRelease;
  AllocatorGetRemainingFunction *getRemaining;
};

function Allocator * allocator_init_default (void);
function Allocator * allocator_init (U64 size);
function B8   allocator_free (Allocator * a);
function void * allocator_alloc_aligned (Allocator * a, U64 size);
function void allocator_rollback (Allocator * a, U64 size);


function TempAllocator allocator_temp_create (Allocator * a);
function void * allocator_temp_allocate (TempAllocator * t, U64 size);
function void allocator_temp_release (TempAllocator t);
function U64 allocator_get_remaining (Allocator * a);

function void assign_function_table (Allocator *a, AllocatorFunctionTable *t);
function void construct_function_table(AllocatorFunctionTable *t,
                                       AllocatorInitDefualtFunction *initDefault,
                                       AllocatorInitFunction *init,
                                       AllocatorFreeFunction *free,
                                       AllocateAlignedFunction *allocate,
                                       AllocateRollbackFunction *rollBack,
                                       AllocatorCreateTempFunction *createTemp,
                                       AllocatorTempAllocateFunction *tempAllocate,
                                       AllocatorTempRelaseFunction *tempRelease,
                                       AllocatorGetRemainingFunction *getRemaining);

/*
* --------------------------------------
* General purpose Allocator API
* --------------------------------------
*
*/
// Initialization
#define CAT(a,b) CAT2(a,b) 
#define CAT2(a,b) a##b 
#define UNIQUE_ID() CAT(_uid_, __COUNTER__)
#define CreateFunctionTable(a, id) \
  AllocatorFunctionTable id = { 0 }; \
  construct_function_table((&id), \
                           allocator_init_default, \
                           allocator_init, \
                           allocator_free, \
                           allocator_alloc_aligned, \
                           allocator_rollback, \
                           allocator_temp_create, \
                           allocator_temp_allocate, \
                           allocator_temp_release, \
                           allocator_get_remaining); \
  assign_function_table(a, &id);

#define AllocInitDefault(a, id) allocator_init_default(); CreateFunctionTable((a), (id));
#define AllocInit(size, a, id)  allocator_init((size)); CreateFunctionTable((a), (id));
// Allocate/deallocate based on type
#define Alloc(a, type, count) (type *) allocator_alloc_aligned(a, (U64)(sizeof(type)) * (count))
#define Dealloc(a, type, count) allocator_rollback(a, (U64)(sizeof(type)) * (count))
// Raw size (in bytes) allocation/dallocation
#define AllocRawSize(a, size) allocator_alloc_aligned(a, (U64)size)
#define DeallocRawSize(a, size) allocator_rollback(a, (U64)size)
// Free the allocator when we're done with it
#define AllocatorFree(a) allocator_free(a)
  

/*
* --------------------------------------
* Utilities for Allocator API
* --------------------------------------
*
*/
// Get remaining amount of memory
#define GetRemaining(a) allocator_get_remaining(a)
// Create temp allocator
#define GetTemp(a) allocator_temp_create(a)
// Allocate to the temp allocator
#define TempAlloc(t, type, count) allocator_temp_allocate(&t, (U64)(sizeof(type)) * (count))
#define TempAllocRawSize(t, size) AllocRawSize(t.a, size)
// Release temp allocator
#define ReleaseTemp(t) allocator_temp_release(t)


#endif //_LINEAR_ALLOCATOR_H
