/* date = April 24th 2023 2:21 pm */

#ifndef _DYNAMIC_ALLOCATOR_H
#define _DYNAMIC_ALLOCATOR_H


#include "..\data-structures\RedBlack Tree\rb.h"


/*
* "Free List" style allocator for dynamic allocations.
* 
* This allocator uses a Red-black tree (defined above) to keep track of
* the free contiguous blocks of memory along with its size.
*
*/
typedef struct Header Header;
struct Header 
{
  U64 blockSize;
  U64 padding;
};

typedef struct Pool Pool;
struct Pool
{
  void  *base;
  U64   _size;
  U64    used;
  U64   remaining;
  Tree  *tree;
  HANDLE handle;
};

typedef struct FreeListNode FreeListNode;
struct FreeListNode 
{
  FreeListNode *next;
};

typedef struct FreeList FreeList;
struct FreeList
{
};

typedef uintptr_t UPTR;

/*
* Functions
*
*/
function Pool * pool_init (Tree *t, U64 size);
function bool is_power_of_two (UPTR x);
function U64 calc_padding_with_header (UPTR ptr, UPTR alignment, U64 header_size);
function TreeNode * find_fit(Pool *pool, U64 size);
function void * pool_alloc (Pool *pool, U64 size, U64 alignment);
function void pool_free (Pool *pool, void *data);
function void pool_coalesce (Pool *pool, TreeNode *prevNode);
#endif //_DYNAMIC_ALLOCATOR_H
