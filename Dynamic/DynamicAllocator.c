function Pool * 
pool_init (Tree *t, U64 size)
{ 
  assert (POWER_OF_TWO (size));
  Pool *p = 0;
  HANDLE win32HeapHandle = HeapCreate (0, sizeof(Pool), 0);
  if (!win32HeapHandle) 
  {
    printf("Failed to create private heap HANDLE for allocator use.\n");
    printf("Defaulting to GetProcessHeap() for allocator usage.\n");
    p = (Pool *) HeapAlloc (GetProcessHeap(), HEAP_ZERO_MEMORY, size);
  } else 
  {
    p = (Pool *) HeapAlloc (win32HeapHandle, HEAP_ZERO_MEMORY, size);
  }
  
  if (!p)
  {
    printf ("Failed to reserve memory block for size : %" PRIu64 "\n", size);
    return NULL;
  } else 
  {
    p->base  = (void *) (p + sizeof(Pool));
    p->_size = size;
    p->tree  = t;
    p->used  = 0;
    p->handle = win32HeapHandle != NULL ? win32HeapHandle : GetProcessHeap();
    p->remaining = size;
    return (p);
  }
}


function bool
is_power_of_two (UPTR x)
{
  return (x & (x - 1)) == 0;
}


function U64
calc_padding_with_header (UPTR ptr, UPTR alignment, U64 header_size)
{
  UPTR p, a, modulo, padding, needed_space;
  
  assert (is_power_of_two (alignment));
  
  p = ptr;
  a = alignment;
  modulo = p & (a - 1); // (p % a) as it assumes alignment is a power of two
  
  padding = 0;
  needed_space = 0;
  
  if (modulo != 0)
  {                    // Same logic as 'align_forward'
    padding = a - modulo;
  }
  
  needed_space = (UPTR) header_size;
  
  if (padding < needed_space)
  {
    needed_space -= padding;
    
    if ((needed_space & (a - 1)) != 0)
    {
      padding += a * (1 + (needed_space / a));
    } else
    {
      padding += a * (needed_space / a);
    }
  }
  
  return ((U64) padding);
}


function UPTR
align_forward (UPTR ptr, U8 align)
{
  UPTR p, a, mod;
  
  assert (is_power_of_two (align));
  
  p = ptr;
  a = (UPTR) align;
  
  mod = p & (a - 1); // GBill: faster than (p % a) since a is gaurenteed to be a power of 2
  
  // if p is NOT aligned then we push the ptr addr to the next value
  if (mod != 0)
  {
    p += a - mod;
  }
  
  return (p);
}


function TreeNode * 
find_fit(Pool *pool, 
         U64 size)
{
  TreeNode *node = NULL;
  
  //search
  
  if (node)
  {
    return (node);
  } else 
  {
    return NULL;
  }
}


function void * 
pool_alloc (Pool *pool, 
            U64 size, 
            U64 alignment)
{
  
}


function void 
pool_free (Pool *pool, void *data)
{
  
}


function void 
pool_coalesce (Pool *pool, TreeNode *prevNode)
{
  
}